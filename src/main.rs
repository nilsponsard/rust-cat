use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

fn help() {
    println!("no arguments");
}

fn main() -> std::io::Result<()> {
    let mut modes: Vec<bool> = vec![false, false, false, false];
    let mut args: Vec<String> = env::args().collect();
    args = args[1..].to_vec();
    let mut files_to_read: Vec<File> = Vec::new();
    if args.len() < 1 {
        help();
    } else {
        let path = std::env::current_dir()?;
        for arg in args {
            if path.join(&arg).exists() {
                let file = File::open(path.join(&arg))?;
                files_to_read.push(file);
            } else if arg.len() > 0 && arg.starts_with("-") {
                // it’s a parameter
                match arg.as_str() {
                    "--help" => {
                        help();
                        modes[0] = true;
                    }
                    "-n" | "--number" => modes[1] = true,
                    "-b" | "--number-nonblank" => modes[2] = true,
                    "-E" | "--show-ends" => modes[3] = true,
                    _ => (),
                }
            } else {
                eprintln!("{} : fichier introuvable", arg);
            }
        }
        let mut n = 0;
        if !modes[0] {
            for mut file in files_to_read {
                let mut contents = String::new();
                file.read_to_string(&mut contents)?;
                let split = contents.split("\n");
                for l in split {
                    let line = String::from(l);
                    if modes[1] || modes[2] {
                        if line.len() > 0 || (!modes[2] && modes[1]) {
                            n += 1;
                            print!("{:>6}  ", n);
                        }
                    }
                    print!("{}", line.replace("\r", ""));
                    if modes[3] {
                        print!("$");
                    }
                    print!("\n")
                }
            }
        }
    }

    Ok(())
}
